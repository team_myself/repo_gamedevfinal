﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    public float fltHealth = 100f;
    public Slider healthSlider;

    /// <summary>
    /// Reduce the health of the player when he is hit
    /// </summary>
    /// <param name="inputDamage"></param>
    public void TakeHit(float inputDamage) {
        fltHealth -= inputDamage;
        healthSlider.value = fltHealth;

        if (fltHealth < 1) {
            GameManager.GetInstance().TriggerGameOver();
            Debug.Log("GAME OVER");
        }
    }
}
