﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MainMenu_Manager : MonoBehaviour {

    // Buttons to be reassigned
    public Button btnSurvLevel1;
    public Button btnCollectLevel1;
    public Button btnQuitYes;

    // Start is called before the first frame update
    void Start() {
        GameManager.GetInstance().isInMainMenu = true;
        GameManager.GetInstance().isGameOver = false;

        // Reassign buttons of the panels
        btnSurvLevel1.onClick.AddListener(GameManager.GetInstance().Survival_Level_1);
        btnCollectLevel1.onClick.AddListener(GameManager.GetInstance().Collector_Level_1);
        btnQuitYes.onClick.AddListener(GameManager.GetInstance().QuitGame);
    }
}
