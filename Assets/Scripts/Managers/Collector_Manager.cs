﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Collector_Manager : MonoBehaviour {

    // References for the Game Manager;
    public TextMeshProUGUI scoreReference;
    public GameObject gameOverReference;
    public GameObject pauseReference;
    public GameObject winReference;
    public GameObject collectiblesReference;

    // Buttons to be reassigned
    public Button btnGameOverRestart;
    public Button btnGameOverMainMenu;
    public Button btnPauseResume;
    public Button btnPauseRestart;
    public Button btnPauseMainMenu;
    public Button btnWinRestart;
    public Button btnWinMainMenu;


    // Start is called before the first frame update
    void Start() {
        // Reassign references for the Game Manager
        GameManager.GetInstance().txtScoreValue = scoreReference;
        GameManager.GetInstance().gameOverScreen = gameOverReference;
        GameManager.GetInstance().pausePanel = pauseReference;
        GameManager.GetInstance().winPanel = winReference;
        GameManager.GetInstance().collectibles = collectiblesReference;

        // Reassign buttons of the panels
        btnGameOverRestart.onClick.AddListener(GameManager.GetInstance().RestartLevel);
        btnGameOverMainMenu.onClick.AddListener(GameManager.GetInstance().LoadMainMenu);
        btnPauseResume.onClick.AddListener(GameManager.GetInstance().UnpauseGame);
        btnPauseRestart.onClick.AddListener(GameManager.GetInstance().RestartLevel);
        btnPauseMainMenu.onClick.AddListener(GameManager.GetInstance().LoadMainMenu);
        btnWinRestart.onClick.AddListener(GameManager.GetInstance().RestartLevel);
        btnWinMainMenu.onClick.AddListener(GameManager.GetInstance().LoadMainMenu);

        // Reset values of variables
        GameManager.GetInstance().fltRockDamage = GameManager.DEFAULT_ROCK_DAMAGE;
        GameManager.GetInstance().intScore = 0;
        GameManager.GetInstance().intMoneyInScene = collectiblesReference.transform.childCount;
        GameManager.GetInstance().txtScoreValue.text = "Money: 0/" + GameManager.GetInstance().intMoneyInScene;
        GameManager.GetInstance().intDifficultyMultiplier = 1;
    }
}
