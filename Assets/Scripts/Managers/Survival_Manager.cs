﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Survival_Manager : MonoBehaviour {

    // References for the Game Manager;
    public TextMeshProUGUI scoreReference;
    public GameObject gameOverReference;
    public GameObject pauseReference;

    // Buttons to be reassigned
    public Button btnGameOverRestart;
    public Button btnGameOverMainMenu;
    public Button btnPauseResume;
    public Button btnPauseRestart;
    public Button btnPauseMainMenu;


    // Start is called before the first frame update
    void Start() {
        GameManager.GetInstance().fltRockDamage = GameManager.DEFAULT_ROCK_DAMAGE;

        // Reassign references for the Game Manager
        GameManager.GetInstance().txtScoreValue = scoreReference;
        GameManager.GetInstance().gameOverScreen = gameOverReference;
        GameManager.GetInstance().pausePanel = pauseReference;

        // Reassign buttons of the panels
        btnGameOverRestart.onClick.AddListener(GameManager.GetInstance().RestartLevel);
        btnGameOverMainMenu.onClick.AddListener(GameManager.GetInstance().LoadMainMenu);
        btnPauseResume.onClick.AddListener(GameManager.GetInstance().UnpauseGame);
        btnPauseRestart.onClick.AddListener(GameManager.GetInstance().RestartLevel);
        btnPauseMainMenu.onClick.AddListener(GameManager.GetInstance().LoadMainMenu);
    }
}
