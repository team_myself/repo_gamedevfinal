﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour {

    #region UI_REFERENCES
    public TextMeshProUGUI txtScoreValue;
    public GameObject gameOverScreen;
    public GameObject pausePanel;
    public GameObject winPanel;
    #endregion

    #region CONTROL_OBJECTS
    public int intScore;
    public GameObject collectibles;
    #endregion

    #region CONTROL_VARIABLES
    public float fltRockDamage = 10f;
    public static float DEFAULT_ROCK_DAMAGE = 10f;

    public bool isCountdownEnded;

    private bool isGamePaused;
    public bool isGameOver;
    public bool isInMainMenu;

    public int intDifficultyMultiplier;
    public float fltTimeToIncreaseDifficulty;

    public int intMoneyInScene;
    #endregion

    #region SCENES
    public Scene mainMenu;
    public Scene survLevel1;
    public Scene collectLevel1;
    
    #endregion

    #region LAZY_SINGLETON
    private static GameManager instance;

    public static GameManager GetInstance() {
        if (instance != null) {
            return instance;
        } else {
            return null;
        }
    }

    private void Awake() {

        if (instance == null) {
            instance = this;
        } else if (instance != this) {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }
    #endregion

    // Update is called once per frame
    private void Update() {
        if (Input.GetButtonDown("Cancel") && !isInMainMenu && !isGameOver) { // Pause does not work in the main menu
            CheckPauseState();
        }
    }

    /// <summary>
    /// Game over if the player dies
    /// </summary>
    public void TriggerGameOver() {
        Time.timeScale = 0f;
        isGameOver = true;
        gameOverScreen.SetActive(true);
    }

    /// <summary>
    /// Win when all the money is collected
    /// </summary>
    public void TriggerGameWin() {
        Time.timeScale = 0f;
        isGameOver = true;
        winPanel.SetActive(true);
    }

    /// <summary>
    /// Increase score when money is collected
    /// </summary>
    public void OnMoneyCollected() {
        intScore++;
        txtScoreValue.text = "Money: " + intScore.ToString() + "/" + intMoneyInScene;
        if (intScore >= intMoneyInScene) {
            TriggerGameWin();
        }
    }

    /// <summary>
    /// When the Pause key is pressed, check if the game is being paused or unpaused
    /// </summary>
    private void CheckPauseState() {
        isGamePaused = !isGamePaused;

        if (isGamePaused) {
            PauseGame();
        } else {
            UnpauseGame();
        }
    }

    public void PauseGame() {
        Time.timeScale = 0f;
        pausePanel.SetActive(true);
        //Cursor.lockState = CursorLockMode.None;
    }

    public void UnpauseGame() {
        Time.timeScale = 1f;
        pausePanel.SetActive(false);
        //Cursor.lockState = CursorLockMode.Locked;
        isGamePaused = false;
    }

    #region SCENE MANAGEMENT FUNCTIONS

    public void RestartLevel() {
        Time.timeScale = 1f;
        isGameOver = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Survival_Level_1() {
        Time.timeScale = 1f;
        isInMainMenu = false;
        SceneManager.LoadScene(survLevel1.handle);
    }

    public void Collector_Level_1() {
        Time.timeScale = 1f;
        isInMainMenu = false;
        SceneManager.LoadScene(collectLevel1.handle);
    }

    public void LoadMainMenu() {
        Time.timeScale = 1f;
        SceneManager.LoadScene(mainMenu.handle);
    }

    public void QuitGame() {
        Application.Quit();
        Debug.Log("Quitting Game");
    }
    #endregion
}
