﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Cronometer : MonoBehaviour {

    public int intStartTime = 0;
    private float intCurrentTimerVal;

    // Control time difficulty interval
    private float fltMinSTDTime;
    private float fltMaxSTDTime;
    private const float STD_OF_TIME_INTERVAL = 0.5f;

    public TextMeshProUGUI cronometerValue;
    public TextMeshProUGUI timeScoreValue;

    public GameManager gameMng;

    // Start is called before the first frame update
    void Start() {
        gameMng = FindObjectOfType<GameManager>();
        GameManager.GetInstance().intDifficultyMultiplier = 1;

        intCurrentTimerVal = intStartTime;
        cronometerValue.text = intCurrentTimerVal.ToString();

        SetTimeOfDifficultyStep();
    }

    // Update is called once per frame
    void Update() {
        // Increase time count
        if (gameMng.isGameOver) {
            timeScoreValue.text = cronometerValue.text;
            return;
        } else { 
            intCurrentTimerVal += Time.deltaTime;
            cronometerValue.text = Mathf.RoundToInt(intCurrentTimerVal).ToString() + " s";

            // Increase difficulty
            if ((intCurrentTimerVal <= fltMaxSTDTime) && (intCurrentTimerVal >= fltMinSTDTime)) {
                IncreaseDifficulty();
            }
        }
    }

    /// <summary>
    /// Sets a comfortable time interval for the update to identify the exact time of difficulty step
    /// </summary>
    private void SetTimeOfDifficultyStep() {
        fltMinSTDTime = gameMng.fltTimeToIncreaseDifficulty - STD_OF_TIME_INTERVAL * Time.deltaTime;
        fltMaxSTDTime = gameMng.fltTimeToIncreaseDifficulty + STD_OF_TIME_INTERVAL * Time.deltaTime;
    }

    /// <summary>
    /// Increases the limits of the time interval by the step value in the Game Manager
    /// </summary>
    private void IncreaseDifficulty() {
        gameMng.intDifficultyMultiplier++;
        gameMng.fltRockDamage += gameMng.fltRockDamage;
        fltMinSTDTime += gameMng.fltTimeToIncreaseDifficulty;
        fltMaxSTDTime += gameMng.fltTimeToIncreaseDifficulty;
    }
}
