﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockSpawner : MonoBehaviour {

    public GameObject rockPrefab;
    public Transform spawnPointStart;
    public Transform spawnPointEnd;
    
    public GameManager gameMng;

    // Time management variables
    public float fltMinRandTime;
    public float fltMaxRandTime;
    public float fltSpawnTime;
    public float fltTimeBetweenSpawn;

    // Start is called before the first frame update
    private void Start() {
        gameMng = FindObjectOfType<GameManager>();
        fltSpawnTime = GenerateRandom(fltMinRandTime, fltMaxRandTime);
    }

    // Update is called once per frame
    private void Update() {
        fltTimeBetweenSpawn += Time.deltaTime;
        if (fltTimeBetweenSpawn > fltSpawnTime) {
            // Set value of time between spawn to 0 and calculate the next time
            fltTimeBetweenSpawn = 0;
            fltSpawnTime = GenerateRandom(fltMinRandTime, fltMaxRandTime);

            SpawnRock();
        }
    }

    /// <summary>
    /// Generate a random float value given the start and end of an interval
    /// </summary>
    /// <param name="inStartInterval"></param>
    /// <param name="inEndInterval"></param>
    /// <returns></returns>
    float GenerateRandom(float inStartInterval, float inEndInterval) {
        return UnityEngine.Random.Range(inStartInterval, inEndInterval);
    }

    /// <summary>
    /// // Spawn the rock in a random x position
    /// </summary>
    private void SpawnRock() {
        GameObject rock = Instantiate(rockPrefab, new Vector3(GenerateRandom(spawnPointStart.position.x, spawnPointEnd.position.x),
                                                              spawnPointStart.position.y,
                                                              spawnPointStart.position.z),
                                      Quaternion.identity);
        // Manipulate the size of the rock based on the current difficulty
        rock.transform.localScale = rock.transform.localScale * GameManager.GetInstance().intDifficultyMultiplier;
        rock.transform.parent = gameObject.transform;
    }

}
