﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockCollision : MonoBehaviour {

    public float fltTimeToDestroy = 1f;

    /// <summary>
    /// Check collision with player and inflict damage
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.CompareTag("Player")) {
            collision.gameObject.GetComponent<Player>().TakeHit(GameManager.GetInstance().fltRockDamage);
        } else if (collision.gameObject.CompareTag(gameObject.tag)) {
            //Debug.Log(collision.gameObject.tag);
            return; // Do not destroy the rock if it collides with another rock
        }
        Destroy(gameObject, fltTimeToDestroy);
    }

}
