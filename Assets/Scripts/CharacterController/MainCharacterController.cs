﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCharacterController : MonoBehaviour {

    public float fltJumpForce = 100f;
    public float fltOverlapRadius = 1.7f;
    public float fltRunForce = 50f;

    public Transform groundCheck; // Ground collision of the character should be here
    public LayerMask groundedLayers; // All layers that you consider as ground

    private Rigidbody2D rbCharacter;
    private Animator animator;
    private SpriteRenderer srCharacter;

    public GameManager gm;
    public Camera mainCamera;
    public float fltCameraX;

    private bool isFacingRight = true; // Character faces right by default
    public bool isAbleToCollect = true;

    // Start is called before the first frame update
    void Start() {
        gm = FindObjectOfType<GameManager>();
        rbCharacter = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        srCharacter = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void FixedUpdate() {
        // Check and return a collider among the layers defined as groundedLayers
        Collider2D groundedResult = Physics2D.OverlapCircle(groundCheck.position, fltOverlapRadius, groundedLayers);

        animator.SetBool("isGrounded", groundedResult); // if there is no collision between the ground groundCheck and a ground layer object, 
                                                                            //the boolean is considered false; 
                                                                            //true otherwise

        float fltHorizontalX = Input.GetAxis("Horizontal");

        // Check the direction of movement
        if (fltHorizontalX < 0f) { // flip the character to the left
            if (isFacingRight) { Flip(); }
        } else if (fltHorizontalX > 0f) { // flip the character to the right
            if (!isFacingRight) { Flip(); }
        }

        // Make run
        if (Mathf.Abs(fltHorizontalX) > 0f) {
            rbCharacter.AddForce(Vector2.right * fltHorizontalX * fltRunForce);
            //Debug.Log(groundedResult);
            animator.SetFloat("vx", Mathf.Abs(rbCharacter.velocity.x));
        }

        // Jump
        if (Input.GetKeyDown(KeyCode.Space) && groundedResult) {
            rbCharacter.AddForce(transform.up * fltJumpForce, ForceMode2D.Impulse);
            // animator.SetBool("isGrounded", false);
        }

    }

    /// <summary>
    /// Flip the x position of the sprite
    /// </summary>
    void Flip () {
        srCharacter.flipX = !srCharacter.flipX;
        isFacingRight = !isFacingRight;
    }

    /// <summary>
    /// When the player enters a collision zone near a wall, register the x position of the camera; Collect gold game objects
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision) {
        // When money is collected, increase the score and destroy the money
        if (collision.gameObject.CompareTag("Money") && isAbleToCollect) {
            Debug.Log("Money Collected!");
            isAbleToCollect = false;
            StartCoroutine(LateCall()); // Wait 0.5 s to collect the next coin (avoids collections to be registered more than once)
            Destroy(collision.gameObject);
            gm.OnMoneyCollected();
        }

        // Fix the camera position when close to the walls
        if (collision.gameObject.CompareTag("Wall")) {
            //Debug.Log("Near wall");
            fltCameraX = mainCamera.transform.position.x;
            //mainCamera.transform.parent = null;
        }
    }

    /// <summary>
    /// Wait for 0.5 second to be able to collect again
    /// </summary>
    /// <returns></returns>
    IEnumerator LateCall() {
        yield return new WaitForSeconds(0.5f);
        isAbleToCollect = true;
    }

    /// <summary>
    /// While the player is near a wall, freeze the x position of the camera
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerStay2D(Collider2D collision) {
        if (collision.CompareTag("Wall")) {
            //Debug.Log("Exiting wall");
            mainCamera.transform.position = new Vector3(fltCameraX, mainCamera.transform.position.y, mainCamera.transform.position.z);
            //mainCamera.transform.SetParent(gameObject.transform);
        }
    }

    /// <summary>
    /// Kill the player if they left the game area by falling into holes
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.CompareTag("Finish")) {
            GameManager.GetInstance().TriggerGameOver();
        }
    }
}
